
function routeTC() {
    window.location.href = "#ALVIS-1";
}

function routeEC() {
    window.location.href = "#ALVIS-2";
}

function routeSC() {
    window.location.href = "#ALVIS-3";
}

function routePTZ() {
    window.location.href = "#ALVIS-4";
}

function routeEP() {
    window.location.href = "#ALVIS-5";
}

function TCInViewport() {
    var myElement = document.getElementById('ALVIS-TC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function ECInViewport() {
    var myElement = document.getElementById('ALVIS-EC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function SCInViewport() {
    var myElement = document.getElementById('ALVIS-SC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function PTZCInViewport() {
    var myElement = document.getElementById('ALVIS-PTZC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function EPInViewport() {
    var myElement = document.getElementById('ALVIS-EP');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

window.addEventListener("scroll", checkRadio);

function checkRadio() {
    if (TCInViewport() == true) {
        const lc1 = document.getElementById("route-1");
        const lc2 = document.getElementById("route-2");
        const lc3 = document.getElementById("route-3");
        const lc4 = document.getElementById("route-4");
        const lc5 = document.getElementById("route-5");
        lc1.checked = true;
        lc1.className = "radio-route";
        lc2.className = "radio-route";
        lc3.className = "radio-route";
        lc4.className = "radio-route";
        lc5.className = "radio-route";
        
    }
    else if (ECInViewport() == true) {
        const lc1 = document.getElementById("route-1");
        const lc2 = document.getElementById("route-2");
        const lc3 = document.getElementById("route-3");
        const lc4 = document.getElementById("route-4");
        const lc5 = document.getElementById("route-5");
        lc2.checked = true;
        lc1.className = "radio-route";
        lc2.className = "radio-route";
        lc3.className = "radio-route";
        lc4.className = "radio-route";
        lc5.className = "radio-route";
    }
    else if (SCInViewport() == true) {
        const lc1 = document.getElementById("route-1");
        const lc2 = document.getElementById("route-2");
        const lc3 = document.getElementById("route-3");
        const lc4 = document.getElementById("route-4");
        const lc5 = document.getElementById("route-5");
        lc3.checked = true;
        lc1.className = "radio-route2";
        lc2.className = "radio-route2";
        lc3.className = "radio-route2";
        lc4.className = "radio-route2";
        lc5.className = "radio-route2";
    }
    else if (PTZCInViewport() == true) {
        const lc1 = document.getElementById("route-1");
        const lc2 = document.getElementById("route-2");
        const lc3 = document.getElementById("route-3");
        const lc4 = document.getElementById("route-4");
        const lc5 = document.getElementById("route-5");
        lc4.checked = true;
        lc1.className = "radio-route2";
        lc2.className = "radio-route2";
        lc3.className = "radio-route2";
        lc4.className = "radio-route2";
        lc5.className = "radio-route2";
    }
    else if (EPInViewport() == true) {
        const lc1 = document.getElementById("route-1");
        const lc2 = document.getElementById("route-2");
        const lc3 = document.getElementById("route-3");
        const lc4 = document.getElementById("route-4");
        const lc5 = document.getElementById("route-5");
        lc5.checked = true;
        lc1.className = "radio-route2";
        lc2.className = "radio-route2";
        lc3.className = "radio-route2";
        lc4.className = "radio-route2";
        lc5.className = "radio-route2";
    }
    else {
        void(0);
    }    
}